package my.utar.com.todolistmanager;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

import org.joda.time.DateTime;

import java.io.Serializable;

public class DatePickerFragment extends DialogFragment{

    //Workaround to implement the OnDateSet listener in caller activity/fragment
    static DatePickerFragment newInstance(Serializable listener){
        DatePickerFragment f = new DatePickerFragment();

        Bundle args = new Bundle();
        args.putSerializable("listener", listener);

        f.setArguments(args);
        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        DateTime date = new DateTime();

        DatePickerDialog.OnDateSetListener listener = (DatePickerDialog.OnDateSetListener) getArguments().getSerializable("listener");
        DatePickerDialog datePicker = new DatePickerDialog(getActivity(), listener, date.getYear(), date.getMonthOfYear()-1, date.getDayOfMonth());
        //Disable date in the past
        datePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        return datePicker;
    }
}
