package my.utar.com.todolistmanager;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import java.io.Serializable;

public class EditEventActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener, Serializable {
    private TDLDatabase db;
    private static Serializable self;
    int idSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_event);
        db = new TDLDatabase(this);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //hide the toolbar spinner by default
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setVisibility(View.INVISIBLE);
        db = new TDLDatabase(EditEventActivity.this);
        self = this;

        idSelected = getIntent().getIntExtra("IDSelected", 0);
        initializeEditForm();
    }

    public void initializeEditForm() {
        db.open();
        Cursor c = db.getEvent(idSelected);
        db.close();

        EditText name = (EditText) findViewById(R.id.name);
        EditText detail = (EditText) findViewById(R.id.detail);
        EditText category = (EditText) findViewById(R.id.category);
        EditText dueDate = (EditText) findViewById(R.id.due_date);
        EditText dueTime = (EditText) findViewById(R.id.due_time);
        Button btnEditEvent = (Button) findViewById(R.id.edit);

        name.setText(c.getString(1));
        detail.setText(c.getString(3));
        category.setText(c.getString(2));
        dueDate.setText(c.getString(4));
        dueTime.setText(c.getString(5));

        category.setOnFocusChangeListener(getCategoryListener());
        category.setOnClickListener(getCategoryClickListener());
        dueDate.setOnFocusChangeListener(getDateInputListener());
        dueDate.setOnClickListener(getDateClickListener());
        dueTime.setOnFocusChangeListener(getTimeInputListener());
        dueTime.setOnClickListener(getTimeClickListener());
        btnEditEvent.setOnClickListener(getEditButtonClickListener());
    }

    public void updateEvent(View view) {
        String name = ((EditText) findViewById(R.id.name)).getText().toString();
        String category = ((EditText) findViewById(R.id.category)).getText().toString();
        String detail = ((EditText) findViewById(R.id.detail)).getText().toString();
        String date = ((EditText) findViewById(R.id.due_date)).getText().toString();
        String time = ((EditText) findViewById(R.id.due_time)).getText().toString();
        if (name.length() > 0 && category.length() > 0 && detail.length() > 0 && date.length() > 0 && time.length() > 0) {
            db.open();
            db.updateEvent(idSelected, name, detail, category, date, time);
            db.close();

            Toast.makeText(this, "Event updated", Toast.LENGTH_SHORT).show();
            //destroy current activity
            this.finish();
        } else {
            Toast.makeText(this, "Some field are empty...", Toast.LENGTH_SHORT).show();
        }
    }

    public View.OnClickListener getEditButtonClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateEvent(v);
            }
        };
    }

    public void selectCategory(View view) {
        CategoryDialogSelection categoryDialog = new CategoryDialogSelection();
        categoryDialog.show(getFragmentManager(), "category");
    }

    public View.OnClickListener getCategoryClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectCategory(v);
            }
        };
    }

    //Category Event Listener
    public View.OnFocusChangeListener getCategoryListener() {
        return new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    selectCategory(v);
            }
        };
    }

    //Open the dialog
    public static class CategoryDialogSelection extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.category)
                    .setItems(R.array.category, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            EditText category = (EditText) getActivity().findViewById(R.id.category);
                            String[] categories = getResources().getStringArray(R.array.category);
                            category.setText(categories[which]);
                        }
                    });
            return builder.create();
        }
    }

    //Time Input Event Listener
    public void selectTime(View v) {
        TimePickerFragment picker = new TimePickerFragment().newInstance(self);
        picker.show(getFragmentManager(), "dueTimePicker");
    }

    public View.OnClickListener getTimeClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectTime(v);
            }
        };
    }

    public View.OnFocusChangeListener getTimeInputListener() {
        return new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    selectTime(v);
            }
        };
    }

    @Override
    public void onTimeSet(TimePicker picker, int hour, int min) {
        EditText time = (EditText) findViewById(R.id.due_time);
        time.setText(new LocalTime(hour, min).toString("HH:mm"));
    }

    //Date Input Event Listener
    public void selectDate(View v) {
        DatePickerFragment picker = new DatePickerFragment().newInstance(self);
        picker.show(getFragmentManager(), "dueDatePicker");
    }

    public View.OnClickListener getDateClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectDate(v);
            }
        };
    }

    public View.OnFocusChangeListener getDateInputListener() {
        return new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    selectDate(v);
            }
        };
    }

    @Override
    public void onDateSet(DatePicker picker, int year, int month, int day) {
        EditText date = (EditText) findViewById(R.id.due_date);
        date.setText(new LocalDate(year, (month + 1), day).toString("YYYY-MM-dd"));
    }
}
