package my.utar.com.todolistmanager;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

public class Event {

    private int id;
    private String title;
    private String detail;
    private String category;
    private LocalDate dueDate;
    private LocalTime dueTime;
    private boolean completed;

    public Event(int id, String title, String detail, String category, LocalDate dueDate, LocalTime dueTime, boolean completed) {
        this.id = id;
        this.title = title;
        this.detail = detail;
        this.category = category;
        this.dueDate = dueDate;
        this.dueTime = dueTime;
        this.completed = completed;
    }

    public int getId() {
        return this.id;
    }

    public String getTitle() {
        return this.title;
    }

    public String getDetail() {
        return this.detail;
    }

    public String getCategory() {
        return this.category;
    }

    public LocalDate getDueDate() {
        return this.dueDate;
    }

    public LocalTime getDueTime() {return this.dueTime; }

    public boolean isCompleted(){
        return completed;
    }
}
