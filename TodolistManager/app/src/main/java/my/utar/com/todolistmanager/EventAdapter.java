package my.utar.com.todolistmanager;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;

import java.util.ArrayList;

public class EventAdapter extends ArrayAdapter<Event> {

    private ArrayList<Event> events;

    public EventAdapter(Context context, int textViewResourceId, ArrayList<Event> list) {
        super(context, textViewResourceId, list);
        this.events = list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.event_list, null);
        }


        Event i = events.get(position);
        if (i != null) {
            Typeface roboto = Typeface.createFromAsset(getContext().getAssets(),
                    "fonts/Roboto-Black.ttf");
            CheckedTextView title = (CheckedTextView) v.findViewById(R.id.title);
            TextView category = (TextView) v.findViewById(R.id.category);
            TextView date = (TextView) v.findViewById(R.id.date);
            TextView time = (TextView) v.findViewById(R.id.time);
            title.setTypeface(roboto);
            title.setChecked(i.isCompleted());
            if (title != null) {
                title.setText(i.getTitle());
            }
            if(category != null){
                category.setText(i.getCategory());
            }
            if(date != null){
                date.setText(i.getDueDate().toString("dd/MM/yyyy"));
            }
            if(time != null){
                time.setText(i.getDueTime().toString("HH:mm"));
            }
        }

        return v;
    }
}
