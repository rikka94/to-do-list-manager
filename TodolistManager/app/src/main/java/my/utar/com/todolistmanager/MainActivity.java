package my.utar.com.todolistmanager;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    private TDLDatabase db;
    private int idSelected;
    private int idSelectedCheck;
    private String currentSelection = "Today";
    EventAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = new TDLDatabase(this);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        addSpinnerContent();
    }

    @Override
    public void onStart() {
        super.onStart();
        ListView listView = (ListView) findViewById(R.id.event_list);
        listView.setChoiceMode(ListView.CHOICE_MODE_NONE);
        listView.setOnItemClickListener(checkCompleteEvent());
        listView.setOnItemLongClickListener(getListViewListener());
        listView.setAdapter(retrieveEvent());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(this, SettingActivity.class);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //Add content to toolbar spinner
    private void addSpinnerContent() {
        String[] spinnerContent = getResources().getStringArray(R.array.when);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, spinnerContent);
        adapter.setDropDownViewResource(R.layout.dropdown_list_content);
        final Spinner sp = (Spinner) findViewById(R.id.spinner);
        sp.setAdapter(adapter);

        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if ("Today".equals(sp.getSelectedItem().toString())) {
                    currentSelection = "Today";
                }
                if ("Tomorrow".equals(sp.getSelectedItem().toString())) {
                    currentSelection = "Tomorrow";
                }
                if ("Yesterday".equals(sp.getSelectedItem().toString())) {
                    currentSelection = "Yesterday";
                }
                if ("Older".equals(sp.getSelectedItem().toString())) {
                    currentSelection = "Older";
                }
                if ("Coming".equals(sp.getSelectedItem().toString())) {
                    currentSelection = "Coming";
                }
                if ("Overdue".equals(sp.getSelectedItem().toString())) {
                    currentSelection = "Overdue";
                }
                onStart();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });
    }


    //Go to add event activity
    public void addEvent(View v) {
        Intent i = new Intent(this, AddEventActivity.class);
        startActivity(i);
    }

    //retrieve all event
    public EventAdapter retrieveEvent() {
        db.open();
        ArrayList<Event> eventList = db.getSelectedEvent(currentSelection);
        db.close();
        adapter = new EventAdapter(this, R.layout.event_list, eventList);
        return adapter;
    }

    //Edit or Delete item on long tapping
    private ListView.OnItemLongClickListener getListViewListener() {
        return new ListView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, final View view, final int position, long id) {
                AlertDialog.Builder actionDialog = new AlertDialog.Builder(MainActivity.this);
                actionDialog.setTitle(R.string.action);
                actionDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                actionDialog.setItems(R.array.action, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        idSelected = adapter.getItem(position).getId();
                        switch (which) {
                            case 0:
                                Intent intent = new Intent(view.getContext(), EditEventActivity.class);
                                intent.putExtra("IDSelected", idSelected);
                                startActivity(intent);
                                break;
                            case 1:
                                db.open();
                                if(db.deleteEvent(idSelected))
                                    Toast.makeText(getBaseContext(), "Event deleted", Toast.LENGTH_SHORT).show();
                                db.close();
                                onStart();
                                break;
                        }
                    }
                });
                actionDialog.create().show();
                return true;
            }
        };
    }

    //Check/Uncheck to indicate completion of tasks
    private ListView.OnItemClickListener checkCompleteEvent() {
        return new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                idSelectedCheck = adapter.getItem(position).getId();
                boolean checkComplete;
                db.open();
                Cursor select = db.getEvent(idSelectedCheck);
                int isChecked = select.getInt(6);

                if (isChecked == 0){
                    checkComplete = true;
                    Toast.makeText(getBaseContext(), "Marked as complete", Toast.LENGTH_SHORT).show();
                }
                else{
                    checkComplete = false;
                    Toast.makeText(getBaseContext(), "Marked as incomplete", Toast.LENGTH_SHORT).show();
                }

                db.completeEvent(idSelectedCheck, checkComplete);
                db.close();
                onStart();
            }
        };
    }
}
