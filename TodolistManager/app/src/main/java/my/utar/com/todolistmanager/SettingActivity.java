package my.utar.com.todolistmanager;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
public class SettingActivity extends AppCompatActivity {

    //Declare list of preference keys here
    static final String NAME = "name";
    static final String REMINDER = "reminder";
    static final String REMINDER_TIME = "reminder_time";
    static final String SOUND = "sound";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new TDLPreferenceFragment()).commit();
    }
}
