package my.utar.com.todolistmanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;

import java.util.ArrayList;

public class TDLDatabase {

    public static final String KEY_ROWID = "id";
    public static final String KEY_NAME = "name";
    public static final String KEY_DETAIL = "detail";
    public static final String KEY_CATEGORY = "category";
    public static final String KEY_DATE = "date";
    public static final String KEY_TIME = "time";
    public static final String KEY_ISCOMPLETE = "completed";
    private static final String DATABASE_NAME = "TDLDB";
    private static final String DATABASE_TABLE = "events";
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_CREATE =
            "create table " + DATABASE_TABLE +"(" + KEY_ROWID + " integer primary key autoincrement, "
                    + KEY_NAME + " TEXT NOT NULL, " + KEY_DETAIL + " TEXT, "
                    + KEY_CATEGORY + " TEXT NOT NULL, " +
                    KEY_DATE + " TEXT NOT NULL, " +
                    KEY_TIME + " TEXT NOT NULL, " +
                    KEY_ISCOMPLETE + " INTEGER DEFAULT 0);";

    private final Context context;
    private DatabaseHelper DBHelper;
    private SQLiteDatabase db;

    public TDLDatabase(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DATABASE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
            onCreate(db);
        }
    }

    //---opens the database---
    public TDLDatabase open() {
        db = DBHelper.getWritableDatabase();
        return this;
    }


    //---closes the database---
    public void close() {
        DBHelper.close();
    }


    //---insert a contact into the database---
    public long insertEvent(String name, String detail, String category, String date, String time) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_NAME, name);
        initialValues.put(KEY_DETAIL, detail);
        initialValues.put(KEY_CATEGORY, category);
        initialValues.put(KEY_DATE, date);
        initialValues.put(KEY_TIME, time);
        return db.insert(DATABASE_TABLE, null, initialValues);
    }


    //---deletes a particular event---
    public boolean deleteEvent(long rowId) {
        return db.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) > 0;
    }


    //---retrieves all the event---
    public Cursor getAllEvent() {
        return db.query(DATABASE_TABLE, new String[]{KEY_ROWID, KEY_NAME, KEY_CATEGORY, KEY_DETAIL, KEY_DATE, KEY_TIME, KEY_ISCOMPLETE},
                null, null, null, null, null);
    }


    //---retrieves a particular event---
    public Cursor getEvent(long rowId) {
        Cursor mCursor = db.query(true, DATABASE_TABLE, new String[]{KEY_ROWID,
                        KEY_NAME, KEY_CATEGORY, KEY_DETAIL, KEY_DATE, KEY_TIME, KEY_ISCOMPLETE}, KEY_ROWID + "=" + rowId, null,
                null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }


    //---updates a contact---
    public boolean updateEvent(long rowId, String name, String detail, String category, String date, String time) {
        ContentValues args = new ContentValues();
        args.put(KEY_NAME, name);
        args.put(KEY_DETAIL, detail);
        args.put(KEY_CATEGORY, category);
        args.put(KEY_DATE, date);
        args.put(KEY_TIME, time);
        return db.update(DATABASE_TABLE, args, KEY_ROWID + "=" + rowId, null) > 0;
    }

    public boolean completeEvent(long rowId, boolean isCompleted) {
        ContentValues args = new ContentValues();
        args.put(KEY_ISCOMPLETE, boolToInt(isCompleted));
        return db.update(DATABASE_TABLE, args, KEY_ROWID + "=" + rowId, null) > 0;
    }

    public static int boolToInt(boolean bool) {
        if (!bool)
            return 0;
        else
            return 1;
    }

    public static boolean intToBool(int intVal) {
        if (intVal == 0)
            return false;
        else
            return true;
    }

    public ArrayList<Event> getSelectedEvent(String select) {
        ArrayList<Event> eventList = new ArrayList<Event>();
        Cursor all = getAllEvent();
        while (all.moveToNext()) {
            LocalDate date = DateTimeFormat.forPattern("YYYY-MM-dd").parseLocalDate(all.getString(4));
            LocalTime time = DateTimeFormat.forPattern("HH:mm").parseLocalTime(all.getString(5));
            Long dateTime = (DateTimeFormat.forPattern("YYYY-MM-dd").parseLocalDateTime(all.getString(4))).toDateTime().getMillis();
            Long currentTime = System.currentTimeMillis();
            Long diff = currentTime - dateTime;
            switch (select) {
                case "Today":
                    if (diff < 86400000 && diff > 0) {
                        Event event = new Event(all.getInt(0), all.getString(1), all.getString(3),
                                all.getString(2), date, time, TDLDatabase.intToBool(all.getInt(6)));
                        eventList.add(event);
                    }
                    break;
                case "Yesterday":
                    if (diff < 172800000 && diff > 86400000) {
                        Event event = new Event(all.getInt(0), all.getString(1), all.getString(3),
                                all.getString(2), date, time, TDLDatabase.intToBool(all.getInt(6)));
                        eventList.add(event);
                    }
                    break;
                case "Tomorrow":
                    if (diff < 0 && diff > -86400000) {
                        Event event = new Event(all.getInt(0), all.getString(1), all.getString(3),
                                all.getString(2), date, time, TDLDatabase.intToBool(all.getInt(6)));
                        eventList.add(event);
                    }
                    break;
                case "Coming":
                    if (diff < -86400000) {
                        Event event = new Event(all.getInt(0), all.getString(1), all.getString(3),
                                all.getString(2), date, time, TDLDatabase.intToBool(all.getInt(6)));
                        eventList.add(event);
                    }
                    break;
                case "Older":
                    if (diff > 172800000) {
                        Event event = new Event(all.getInt(0), all.getString(1), all.getString(3),
                                all.getString(2), date, time, TDLDatabase.intToBool(all.getInt(6)));
                        eventList.add(event);
                    }
                    break;
                case "Overdue":
                    if(diff > 86400000 && !intToBool(all.getInt(6))){
                        Event event = new Event(all.getInt(0), all.getString(1), all.getString(3),
                                all.getString(2), date, time, TDLDatabase.intToBool(all.getInt(6)));
                        eventList.add(event);
                    }
                    break;
                default:
                    throw new IllegalArgumentException("invalid input");

            }
        }
        return eventList;
    }

}
