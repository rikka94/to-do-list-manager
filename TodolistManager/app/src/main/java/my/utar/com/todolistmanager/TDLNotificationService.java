package my.utar.com.todolistmanager;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;

public class TDLNotificationService extends BroadcastReceiver {
    private int mID = 0;

    @Override
    public void onReceive(Context ctx, Intent i) {
        NotificationManager notificationManager = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(ctx);
        Intent nIntent = new Intent(ctx, MainActivity.class);
        nIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        String userName = sp.getString(SettingActivity.NAME, "");
        PendingIntent pIntent = PendingIntent.getActivity(ctx, 0, nIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(ctx)
                        .setSmallIcon(R.drawable.ic_drawer)
                        .setContentTitle("Today ToDo Reminder")
                        .setContentText(userName + "Check your pending tasks now ..")
                        .setContentIntent(pIntent);

        Notification notification = mBuilder.build();
        notification.flags = Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL;
        notification.category = Notification.CATEGORY_EVENT;
        if (sp.getBoolean(SettingActivity.SOUND, false)) {
            notification.defaults |= Notification.DEFAULT_SOUND;
        }
        notificationManager.notify(mID, notification);
        mID++;
    }
}
