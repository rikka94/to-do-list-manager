package my.utar.com.todolistmanager;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.widget.TimePicker;

import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;

import java.io.Serializable;
import java.util.Calendar;

public class TDLPreferenceFragment extends PreferenceFragment implements TimePickerDialog.OnTimeSetListener, SharedPreferences.OnSharedPreferenceChangeListener, Serializable
{
    private Serializable self = this;
    @Override
    public void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        findPreference(SettingActivity.REMINDER_TIME).setDefaultValue("12:00");
        findPreference(SettingActivity.REMINDER_TIME).setOnPreferenceClickListener(getTimeClickListener());
    }

    @Override
    public void onStart(){
        super.onStart();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this.getActivity());
        //Initialize preferences summary
        String name = sp.getString(SettingActivity.NAME, "");
        String reminderTime = sp.getString(SettingActivity.REMINDER_TIME, "");
        if(name.length() > 0)
            findPreference(SettingActivity.NAME).setSummary(name);
        if(reminderTime.length() > 0)
            findPreference(SettingActivity.REMINDER_TIME).setSummary(reminderTime);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key){
        //List of preferences
        switch(key){
            case SettingActivity.NAME:
                findPreference(key).setSummary(sharedPreferences.getString(key, null));
                break;
            case SettingActivity.REMINDER:
                setReminder();
                break;
            case SettingActivity.REMINDER_TIME:
                findPreference(key).setSummary(sharedPreferences.getString(key, null));
                setReminder();
                break;
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        getPreferenceManager().getDefaultSharedPreferences(this.getActivity()).registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause(){
        super.onPause();
        getPreferenceManager().getDefaultSharedPreferences(this.getActivity()).unregisterOnSharedPreferenceChangeListener(this);
    }

    public Preference.OnPreferenceClickListener getTimeClickListener(){
        return new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                TimePickerFragment picker = new TimePickerFragment().newInstance(self);
                picker.show(getFragmentManager(), "timePicker");
                return true;
            }
        };
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        LocalTime time = new LocalTime(hourOfDay, minute);
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();
        editor.putString(SettingActivity.REMINDER_TIME, time.toString("HH:mm"));
        editor.apply();
    }

    public void setReminder(){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this.getActivity());
        String reminderTime = sp.getString(SettingActivity.REMINDER_TIME, "00:00");
        Boolean reminder = sp.getBoolean(SettingActivity.REMINDER, true);

        LocalTime time = DateTimeFormat.forPattern("HH:mm").parseLocalTime(reminderTime);
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, time.getHourOfDay());
        c.set(Calendar.MINUTE, time.getMinuteOfHour());

        //If the reminder time is set to a time earlier than current time , add 1 day to the time..
        if(c.getTimeInMillis() < System.currentTimeMillis()){
            c.add(Calendar.HOUR_OF_DAY, 24);
        }

        AlarmManager am = (AlarmManager) this.getActivity().getSystemService(SettingActivity.ALARM_SERVICE);
        Intent i = new Intent(this.getActivity(), TDLNotificationService.class);

        //If reminder is turned on then set the pending intent , else cancel the pending intent
        if(reminder){
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this.getActivity(), 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
            am.setRepeating(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        }
        else{
            //cancel the reminder
            PendingIntent.getBroadcast(this.getActivity(), 0, i, PendingIntent.FLAG_UPDATE_CURRENT).cancel();
        }
    }
}