package my.utar.com.todolistmanager;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.preference.PreferenceManager;
import org.joda.time.LocalTime;
import java.io.Serializable;

public class TimePickerFragment extends DialogFragment{
    //Workaround to implement the OnTimeSet listener in caller activity/fragment
    static TimePickerFragment newInstance(Serializable listener){
        TimePickerFragment f = new TimePickerFragment();
        //pass in activity that implemented ontimeset listener as a serializable object
        //Allow this time picker fragment to be reused in different activity for different purpose by
        //implement listener in the activity
        Bundle args = new Bundle();
        args.putSerializable("listener", listener);

        f.setArguments(args);
        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        //Set default time to current if no shared preferences is found
        String preferedTime = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(SettingActivity.REMINDER_TIME, null);
        LocalTime time = preferedTime != null ? LocalTime.parse(preferedTime) : LocalTime.now();

        TimePickerDialog.OnTimeSetListener listener = (TimePickerDialog.OnTimeSetListener) getArguments().getSerializable("listener");
        return new TimePickerDialog(getActivity(), listener, time.getHourOfDay(), time.getMinuteOfHour(), true);
    }
}